# -*- coding: utf-8 -*-
#
# KanBanTrac
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 3 of the GNU General Public
# License as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA
#
#


from utils import *
from copy import deepcopy
from genshi.builder import Element, tag
from trac.web.api import parse_arg_list
from trac.wiki.formatter import format_to_html
from trac.resource import Resource

class kanbanbase(object):
    
    def __init__(self, parent, req):
        self.parent = parent
        self.kconf = parent.kconf
        self.req = req
        self.qargs = dict(parse_arg_list(req.query_string))
        self.env = parent.env
        self.db = self.env.get_db_cnx()
        self.log = parent.log
        
##        self.curr_milestone = req.args.get('milestone', None)  ## Name of the milestone or None
        
        self._comp_dict = None
        self._projects = None
        self._projects_tree = None
        self._milestones_dict = None
        self._tickettypes_dict = None
        self._ticket_priorities = None
        self._users_dict = None
        self._kanban = None
        self._milestones_tickets = None
        self._kanban_args = None
        self._proj4comps = None
        
        self.clauses = {}
        self.clauses['closed_tickets'] = '(' + ' and '.join(["status = '%s'" % (el.strip()) for el in self.kconf['ticket_closed_status'].split(',')]) + ')'
        if self.clauses['closed_tickets'] == '()': self.clause['closed_tickets'] = '( 1 )'
    @property
    def proj4comps(self):
        if self._proj4comps is None:
            if self.req.args['module'] == 'milestone':
                self._proj4comps = '.'.join(self._kanban_args['milestone'].split('.')[:-1])
            elif self.req.args['module'] == 'component':
                self._proj4comps = '.'.join(self._kanban_args['component'].split('.')[:-1])
            elif self.req.args['module'] == 'project':
                self._proj4comps = self._kanban_args['project']
            else:
                self._proj4comps = ''
        return self._proj4comps
            
    @property
    def kanban_args(self):
        if self._kanban_args is None:
            self._kanban_args = {'project': None, 'milestone': None, 'component': None, 'owner': None, 'reporter': None, 'replace_cols' : None}
            if self.req.args['module'] == 'milestone':
                self._kanban_args['replace_cols'] = [('milestone', 'component'),]
                self._kanban_args['milestone'] = self.req.args['module_call']
                for el in ['component', 'owner', 'reporter']:
                    self._kanban_args[el] = self.qargs.get(el)
            elif self.req.args['module'] == 'component':
                self._kanban_args['replace_cols'] = [('component', 'milestone'),]
                self._kanban_args['component'] = self.req.args['module_call']
                for el in ['milestone', 'owner', 'reporter']:
                    self._kanban_args[el] = self.qargs.get(el)
            elif self.req.args['module'] == 'project':
                self._kanban_args['project'] = self.req.args['module_call']
                for el in ['milestone','component', 'owner', 'reporter']:
                    self._kanban_args[el] = self.qargs.get(el)
            elif self.req.args['module'] == 'user':
                self._kanban_args['replace_cols'] = [('owner', 'reporter'),]
                self._kanban_args['owner'] = self.req.args['module_call']
                for el in ['milestone', 'component', 'reporter']:
                    self._kanban_args[el] = self.qargs.get(el)
            elif self.req.args['module'] == 'global':
                for el in ['milestone', 'component', 'reporter', 'owner', 'project']:
                    self._kanban_args[el] = self.qargs.get(el)
        #self.log.debug("kanban_args = %s" % (self._kanban_args))
        return self._kanban_args
    
    @property
    def components_dict(self):
        if self._comp_dict is None:
            sql = "select name, owner, description, (select count(*) from ticket where component = name) as 'tickets', \
                    (select count(*) from ticket where component = name and %s) as 'closed_tickets' from component" % (self.clauses['closed_tickets'])
            #self.log.debug("components_sql = %s" % (sql))
            cursor = self.db.cursor()
            cursor.execute(sql)
            self._comp_dict = {}
            for comp in cursor:
                self._comp_dict[comp[0]] = {}
                self._comp_dict[comp[0]]['name'] = comp[0]
                self._comp_dict[comp[0]]['owner'] = comp[1]
                self._comp_dict[comp[0]]['description'] = comp[2]
                self._comp_dict[comp[0]]['total_tickets'] = comp[3]
                self._comp_dict[comp[0]]['closed_tickets'] = comp[4]
                self._comp_dict[comp[0]]['q_complete'] = "(%i/%i)" %(comp[4], comp[3])
                if comp[3] == 0:
                    self._comp_dict[comp[0]]['percent'] = 0
                else:
                    self._comp_dict[comp[0]]['percent'] = (100.0 * comp[4]/comp[3])
                self._comp_dict[comp[0]]['links'] = self.component_new_ticket_links_dict(comp[0], self.kanban_args['milestone'])
                self._comp_dict[comp[0]]['href'] = self.req.href.kanbantrac('/component/' + comp[0])
            #self.log.debug('components_dict = %s' % (self._comp_dict))
        return self._comp_dict
    
    @property
    def milestones_dict(self):
        if self._milestones_dict is None:
            self._milestones_dict = {}
            sql = "select name, due, completed, description, (select count(*) from ticket where milestone = name) as 'tickets', \
                    (select count(*) from ticket where milestone = name and %s) as 'closed_tickets' from milestone" %(self.clauses['closed_tickets'])
            cursor = self.db.cursor()
            cursor.execute(sql)
            for el in cursor:
                self._milestones_dict[el[0]] = {}
                self._milestones_dict[el[0]]['name'] = el[0]
                self._milestones_dict[el[0]]['due'] = el[1]
                self._milestones_dict[el[0]]['completed'] = el[2]
                self._milestones_dict[el[0]]['description'] = el[3]
                self._milestones_dict[el[0]]['total_tickets'] = el[4]
                self._milestones_dict[el[0]]['closed_tickets'] = el[5]
                self._milestones_dict[el[0]]['ms_href'] = self.req.href.milestone(el[0])
                self._milestones_dict[el[0]]['q_complete'] = "(%i/%i)" %(el[5], el[4])
                if el[4] == 0:
                    self._milestones_dict[el[0]]['percent'] = 0
                else:
                    self._milestones_dict[el[0]]['percent'] = (100.0 * el[5]/el[4])
                self._milestones_dict[el[0]]['href'] = self.req.href.kanbantrac('milestone/' + el[0])
                self._milestones_dict[el[0]]['resource'] = Resource('milestone', el[0])
            self.db.commit()
        return self._milestones_dict
    
    @property
    def users_dict(self):
        '''
        Select authors which modified a ticket or who reported a ticket
        Syntax for performing left outer joins are database dependent so I prefer 
        two independent queries and then gather the results manually. 
        '''
        if self._users_dict is None:
            self._users_dict = {}
            sql = "select distinct author, (select count(*) from ticket where owner = author) as 'tickets', \
                    (select count(*) from ticket where owner = author and %s) as 'closed_tickets' from ticket_change" %(self.clauses['closed_tickets'])
            cursor = self.db.cursor()
            cursor.execute(sql)
            for el in cursor:
                self._users_dict[el[0]] = {}
                self._users_dict[el[0]]['name'] = el[0]
                self._users_dict[el[0]]['total_tickets'] = el[1]
                self._users_dict[el[0]]['closed_tickets'] = el[2]
                self._users_dict[el[0]]['q_complete'] = "(%i/%i)" %(el[2], el[1])
                if el[1] == 0:
                    self._users_dict[el[0]]['percent'] = 0
                else:
                    self._users_dict[el[0]]['percent'] = (100.0 * el[2]/el[1])
                self._users_dict[el[0]]['href'] = self.req.href.kanbantrac('user/' + el[0])
            sql = "select distinct reporter, (select count(*) from ticket where owner = reporter) as 'tickets', \
                    (select count(*) from ticket where owner = reporter and %s) as 'closed_tickets' from ticket" %(self.clauses['closed_tickets'])
            cursor.execute(sql)
            for el in cursor:
                if el[0] not in self._users_dict.keys(): 
                    self._users_dict[el[0]] = {}
                    self._users_dict[el[0]]['name'] = el[0]
                    self._users_dict[el[0]]['total_tickets'] = el[1]
                    self._users_dict[el[0]]['closed_tickets'] = el[2]
                    self._users_dict[el[0]]['q_complete'] = "(%i/%i)" %(el[2], el[1])
                    if el[1] == 0:
                        self._users_dict[el[0]]['percent'] = 0
                    else:
                        self._users_dict[el[0]]['percent'] = (100.0 * el[2]/el[1])
                    self._users_dict[el[0]]['href'] = self.req.href.kanbantrac('user/' + el[0])
                
            
            self.db.commit()
            self.log.debug("users = %s" % (str(self._users_dict)))
        return self._users_dict

    @property
    def tickettypes_dict(self):
        if self._tickettypes_dict is None:
            rows = ['name', 'value']
            table = 'enum'
            where = 'type = "ticket_type"'
            self._tickettypes_dict = get_from_db_as_dict(self.db, rows, table, where, ix_row = 'value')
        return self._tickettypes_dict
        
        
    @property
    def projects_tree(self):
        max_sublevel = self.kconf['project_subsections']
        if self._projects_tree is None:
            self._projects_tree = {}
            subproj = self._projects_tree
            for el in self.milestones_dict:
                splitted = el.split('.')
                if max_sublevel < len(splitted):
                    ms_name = '.'.join(splitted[max_sublevel:])
                else:
                    ms_name = splitted[-1]
                #self.log.info("milestone: %s, name: %s"%(el, ms_name))
                for i, step in enumerate(splitted[:-1]):
                    if step not in subproj.keys():
                        subproj[step] = {'milestones':{}, 
                                         'components':{}, 
                                         'subprojects':{}, 
                                         'is_last_section': (i==(max_sublevel-1)),
                                         'breadcrumbs' : splitted[:i+1],
                                         'closed_tickets': self.milestones_dict[el]['closed_tickets'],
                                         'total_tickets': self.milestones_dict[el]['total_tickets'],
                                         }
                    else:
                        subproj[step]['closed_tickets'] += self.milestones_dict[el]['closed_tickets']
                        subproj[step]['total_tickets'] += self.milestones_dict[el]['total_tickets']
                    subproj = subproj[step]
                    #self.log.info("step: %s, subproj: %s"%(step, str(subproj)))
                    if (i < len(splitted) - 2) and (i < max_sublevel): 
                        subproj = subproj['subprojects']
                    else:
                        break
                if len(splitted[:-1]) > 0:
                    ## If there is a milestone that doesn't contain '.' in its name, but we are using structured_names
                    subproj['milestones'][ms_name] = self.milestones_dict[el]
                else:
                    ## It has a name without dots:
                    if 'unstructured' not in subproj.keys():
                        subproj['unstructured'] = {'milestones':{}, 
                                         'components':{}, 
                                         'subprojects':{}, 
                                         'is_last_section': True,
                                         'breadcrumbs' : ['Projects with unstructured names',],
                                         'closed_tickets': 0,
                                         'total_tickets': 0,
                                         } 
                    subproj['unstructured']['milestones'][el] = self.milestones_dict[el]
                    subproj['unstructured']['closed_tickets'] += self.milestones_dict[el]['closed_tickets']
                    subproj['unstructured']['total_tickets'] += self.milestones_dict[el]['total_tickets']
                subproj = self._projects_tree
            for el in self.components_dict:
                splitted = el.split('.')
                if max_sublevel < len(splitted):
                    c_name = '.'.join(splitted[max_sublevel:])
                else:
                    c_name = splitted[-1]  
                for i, step in enumerate(splitted[:-1]):
                    if step not in subproj.keys():
                        subproj[step] = {'milestones':{}, 
                                         'components':{}, 
                                         'subprojects':{}, 
                                         'is_last_section': (i==(max_sublevel-1)),
                                         'breadcrumbs' : splitted[:i+1],
                                         'total_tickets' : 0,
                                         'closed_tickets' : 0,
                                         }
                    subproj = subproj[step]
                    if (i < len(splitted) - 2) and (i < max_sublevel): 
                        subproj = subproj['subprojects']
                    else:
                        break
                if len(splitted[:-1]) > 0:
                    ## If there is a milestone that doesn't contain '.' in its name, but we are using structured_names
                    subproj['components'][c_name] = self.components_dict[el]
                else:
                    ## It has a name without dots:
                    if 'unstructured' not in subproj.keys():
                        subproj['unstructured'] = {'milestones':{}, 
                                         'components':{}, 
                                         'subprojects':{}, 
                                         'is_last_section': True,
                                         'breadcrumbs' : splitted[:],
                                         'closed_tickets': 0,
                                         'total_tickets': 0,
                                         } 
                    subproj['unstructured']['components'][el] = self.components_dict[el]
                
                subproj = self._projects_tree
        return self._projects_tree
        
    @property
    def projects(self):
        if self._projects is None:
            self._projects = {}
            for el in self.milestones_dict:
                splitted = el.split('.')
                proj_name = '.'.join(splitted[:-1])
                if proj_name not in self._projects.keys():
                    self._projects[proj_name] = {'milestones':{}, 'components':{}, 'total_tickets' : 0, 'closed_tickets' : 0}
                    self._projects[proj_name]['href'] = self.req.href.kanbantrac('project/' + proj_name)
                self._projects[proj_name]['milestones'] = self.milestones_dict[el]
            for el in self.components_dict:
                splitted = el.split('.')
                proj_name = '.'.join(splitted[:-1])
                if proj_name not in self._projects.keys():
                    self._projects[proj_name] = {'milestones':{}, 'components':{}, 'total_tickets' : 0, 'closed_tickets' : 0}
                    self._projects[proj_name]['href'] = self.req.href.kanbantrac('project/' + proj_name)
                self._projects[proj_name]['components'] = self.components_dict[el]
            sql = "select name, "
            s_total = "select count(*) from ticket where ( component LIKE '%s%%' or milestone LIKE '%s%%')"
            s_done = "select count(*) from ticket where ( component LIKE '%s%%' or milestone LIKE '%s%%') and " + self.clauses['closed_tickets']
            sql += ', '.join(["(" + s_total%(el,el) + ") as '%s_total'" % (el) for el in sorted(self._projects.keys())])
            sql += ', '
            sql += ', '.join(["(" + s_done%(el,el) + ") as '%s_done'" % (el) for el in sorted(self._projects.keys())])
            sql += " from system"
            self.log.debug('sql = %s' % (sql))
            self.log.debug('projects = %s' %(sorted(self._projects.keys())))
            
            cursor = self.db.cursor()
            cursor.execute(sql)
            a = cursor.fetchone()
            for ix, el in enumerate(sorted(self._projects.keys())):
                self._projects[el]['total_tickets'] = a[ix+1]
                self._projects[el]['closed_tickets'] = a[ix+1+len(self._projects.keys())]
                self._projects[el]['q_complete'] = "(%i/%i)"%(a[ix+1+len(self._projects.keys())], a[ix+1])
                if self._projects[el]['total_tickets'] != 0:
                    self._projects[el]['percent'] = (100.0 * self._projects[el]['closed_tickets']/self._projects[el]['total_tickets'])
                else:
                    self._projects[el]['percent'] = 0
            
            self.log.debug("multi_select: %s" % (str(a)))
            self.db.commit()
            
        return self._projects
    

    def component_new_ticket_links_dict(self, component, milestone = None):
        args = {}
        if milestone: args['milestone'] = milestone
        args['component'] = component
        
        links = {}
        for tt in sorted(self.tickettypes_dict):
            args['type'] = self.tickettypes_dict[tt]['name']
            #?type=defect&component=CCS.OCS.ui&milestone=CCS.Core.macros_ready
            qargs = '&'.join(['='.join([el,args[el]]) for el in args])

            links[self.tickettypes_dict[tt]['name']] = self.req.href.newticket() + '?' + qargs
        return links
    
    def components(self, main = False, related = False, others = False, all_comps = False):
        components_dict = self.components_dict
    
        ret_val = {}
        if main or related or others:
            if not self.proj4comps:
                main_comps = []
                project_root = ''
            else:
                main_comps = filter(lambda x: '.'.join(x.split('.')[:-1]).startswith(self.proj4comps) , components_dict.keys())
                project_root = '.'.join(self.proj4comps.split('.')[:-1])
            
            if main:
                ret_val['main'] = {}
                for el in main_comps: ret_val['main'][el] = components_dict[el]
            
            if related:
                if project_root != '':
                    rel_comps = filter(lambda x: x.startswith(project_root) and x not in main_comps, components_dict.keys())
                else:
                    rel_comps = []
                ret_val['related'] = {}
                for el in rel_comps: ret_val['related'][el] = components_dict[el]
                
            if others:
                if not related:
                    if project_root != '':
                        rel_comps = filter(lambda x: x.startswith(project_root) and x not in main_comps, components_dict.keys())
                    else:
                        rel_comps = []
                other_comps = filter(lambda x: x not in rel_comps and x not in main_comps, components_dict.keys()) 
                ret_val['others'] = {}
                for el in other_comps: ret_val['others'][el] = components_dict[el]

        if all_comps:
            ret_val['all'] = components_dict
        ## remove empty dicts
        for el in ret_val.keys():
            if ret_val[el] == {}:
                ret_val.pop(el)
        return ret_val
    
     
    def kanbanreport(self, project = None, milestone = None, component = None, owner = None, reporter = None, replace_cols = None):
        '''
        Create data dict to generate a kanban report
        
        What has to contain a kanban report is set on conf
            self.kconf['bins'] : list of bins and its filters
            self.kconf['bins_modified_filter']: set if a bin has to filter by last modified time
            self.kconf['bins_sections']: which sections has each bin and what filters its tickets
            self.kconf['bins_sections_description']: description of the bin sections
            self.kconf['kanban_report_def_columns'] : default columns shown on the tickets tables on the kanban report
        
        data structure:
        data = [ ## list of bins
            {'columns': ['column', 'names'],
             'description' : '',
             'filter' : ['list of where clauses to apply to bin', u'(status = "new")],
             'name': 'name of the bin',
             'sections':[ ## list of sections
                 {'columns': ['columns', 'names'],
                  'description' : '',
                  'filter': [(type <> "suggestion"),],
                  'name': 'name of the bin section',
                  'table': { 'id': 'id_to_apply_to_table_tag',
                             'sql': 'sql query that generates the data',
                             'table_description': 'whatever you want to print under section title',
                             'table_title': 'title of the section',
                             'tbody':{ ## dict of the body data
                                 'ticket_num': { ## dict of the row data
                                     'td': [ ## list of td data
                                         {'value': 'what you put on td',
                                          'td_class: {##dict with class to apply}
                                         },
                                         {'value': 'what you put on td',
                                          'td_class: {##dict with class to apply on td}
                                         },
                                         ... ]
                                     'tr_class': {##dict with class to apply to tr}
                                     },
                                 'ticket_num2':{},
                                 ...},
                             'tfoot':['column', 'names'],
                             'thead':['column', 'names']
                           }
                 },
                 {##another section dict},
                 ...
                ],
            'width': {'style': 'width:33.33%'}},
           {another bin dict},
           ...]
        '''
        def clause_from_input(data, name, list_of_clauses):
            if data: 
                if isinstance(data, basestring):
                    datal = data.split(',')
                elif isinstance(data, list):
                    datal = data
                else:
                    raise Exception("Input must be either a string or a list")
                list_of_clauses.append(u"(" + ' OR '.join([("%s = '%s'" % (name, str(d))) for d in datal]) + ")")
        kb_filter = []
        clause_from_input(milestone, 'milestone', kb_filter)
        clause_from_input(component, 'component', kb_filter)
        clause_from_input(owner, 'owner', kb_filter)
        clause_from_input(reporter, 'reporter', kb_filter)
        
        def replace_columns(cols, replace = replace_cols):
            self.log.debug("input cols: %s, type: %s" % (cols, type(cols)))
            if replace is None:
                return cols
            for el in replace:
                if el[0] in cols:
                    if el[1] is None:
                        cols.remove(el[0])
                    else:
                        cols[cols.index(el[0])] = el[1]
            return cols
        
        if project: 
            if isinstance(project, basestring):
                projects = project.split(',')
            elif isinstance(project, list):
                projects = project
            else:
                raise Exception("Input must be either a string or a list")
            kb_filter.append("(" + ' OR '.join([("(milestone LIKE '%s%%' OR component LIKE '%s%%')" % (p, p)) for p in projects]) + ')')
        
        if self._kanban is None:
            kanban = []
            kconf = self.env.config['kanbantrac']
            def_cols = kconf.getlist('kr_default_columns')
            for el in kconf.getlist('kr_bins'):
                binname = el.strip()
                kr_bin = {}
                norm_binname = 'kr_bins_' + binname.replace(' ', '.')
                kr_bin['name'] = binname
                kr_bin['description'] = kconf.get(norm_binname + "_desc")
                kr_bin['filter'] = clause_from_filter(kconf.getlist(norm_binname + "_filter")) + kb_filter
                kr_bin['columns'] = replace_columns(kconf.getlist(norm_binname + '_columns', default = def_cols))
                kr_bin['width'] = {'style': "width:%.2f%%"%(100.0/len(kconf.getlist('kr_bins')))}
                #if kr_bin['columns'] == []: kr_bin['columns'] = replace_columns(def_cols)
                kr_bin['ticket_count'] = self.bin_ticket_count(kr_bin['filter'])
                kr_bin['sections'] = []
                for sect in kconf.getlist(norm_binname + "_sections"):
                    sectname = sect.strip()
                    norm_sect = norm_binname + '.' + sectname.replace(' ', '.')
                    section = {}
                    section['name'] = sectname
                    section['description'] = kconf.get(norm_sect + '_desc')
                    section['filter'] = clause_from_filter(kconf.getlist(norm_sect + '_filter')) +  kr_bin['filter']
                    section['columns'] = replace_columns(kconf.getlist(norm_sect + '_columns', default = kr_bin['columns']))
                    section['table'] = self.make_ticket_table_data(section['columns'], 
                                                     section['filter'],
                                                     title = sectname,
                                                     bin_count = kr_bin['ticket_count'], 
                                                     desc = section['description'])
                    #if section['columns'] == []: section['columns'] = replace_columns(kr_bin['columns'])
                    kr_bin['sections'].append(section)
                if kr_bin['sections'] == []:
                    kr_bin['table'] = self.make_ticket_table_data(kr_bin['columns'], kr_bin['filter'], desc = kr_bin['description'])
                kanban.append(kr_bin)
            self._kanban = kanban
            #self.log.debug('kanban = %s' % (str(kanban)))
        return self._kanban
    @property            
    def ticket_priorities(self):
        if not self._ticket_priorities:
            self._ticket_priorities = {}
            cursor = self.db.cursor()
            cursor.execute('select name, value from enum where type="priority"')
            
            for el in cursor:
                self._ticket_priorities[el[0]] = el[1]
            self.db.commit()
            
        return self._ticket_priorities
    
    def bin_ticket_count(self, bin_filter):
        cursor = self.db.cursor()
        if bin_filter:
            clause = ' where ' + ' AND '.join(bin_filter)
        else:
            clause = ''
        cursor.execute('select count(*) from ticket%s' % (clause))
        
        for el in cursor:
            count = el
        
        self.db.commit()
        return count
                       
    def make_ticket_table_data(self, columns, filters = [], title = '', desc = '', bin_count = None, td_as_links = True):
        table = {}
        conv_data = {}
        if 'time' in columns: conv_data[columns.index('time')] = micro_to_data
        if 'changetime' in columns: conv_data[columns.index('changetime')] = micro_to_data 
            
        if title: table['table_title'] = title
        if desc: table['table_description'] = desc
        table['id'] = {'id': 'ticket_table_' + title.strip().replace(' ', '_')}
        table['thead'] = columns
        
        final_cols = columns + ['id', 'priority']
        table['sql'] = 'select ' + ', '.join(final_cols) + ' from ticket'
        if filters: table['sql'] += ' where ' + ' AND '.join(filters)
        
        #self.log.debug("SQL query: %s" % (table['sql']))
        cursor = self.db.cursor()
        cursor.execute(table['sql'])
        
        table['tbody'] = {}
        
        id_ix = final_cols.index('id')
        priority_ix = final_cols.index('priority')
        odd = False
        for el in cursor:
            row = {}
            odd = not odd
            if odd:
                tr_class = 'odd' + ' prio' + str(self.ticket_priorities[el[priority_ix]])
            else:
                tr_class = 'even' + ' prio' + str(self.ticket_priorities[el[priority_ix]])
            row['tr_class'] = {'class': tr_class}
            row['td'] = []
            for i, td_el in enumerate(el[:len(columns)]):
                if i in conv_data.keys():
                    val = conv_data[i](td_el)
                else:
                    val = td_el
                td = {}
                if i != 0: td['td_class'] = {'class': 'center'}
                if td_as_links: td['value']= tag.a(val, href = self.req.href.ticket(el[id_ix]))
                else: td['value'] =  val
                row['td'].append(td)
            table['tbody'][el[id_ix]] = row
        table['counts'] = {'section' : len(table['tbody']),
                           'bin': bin_count}
        if table['tbody'] == {}:
            table['tbody']['na'] = {'td':[{'value':u''} for el in range(len(columns))]}
        table['tfoot'] = columns
        self.db.commit()
        
        return table

        