# -*- coding: utf-8 -*-
#
# KanBanTrac
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 3 of the GNU General Public
# License as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA
#
#


from kanbanbase import kanbanbase

class kanbanIndex(kanbanbase):
    template = None
    data = {}
    
    def make(self):
        if self.kconf['structured_names']:
            self.template = 'projects_index.html'
            self.data['projects'] = self.projects_tree
        else:
            self.template = 'milestones_index.html'
            self.data['milestones'] = self.milestones_dict
        #self.log.info('projects: %s' % (str(self.projects_tree)))
        self.data['component_tickets_foot'] = self.components(all_comps = True)
        
        return self.template, self.data
        


