# -*- coding: utf-8 -*-
#
# KanBanTrac
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 3 of the GNU General Public
# License as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA
#
#

from version import VERSION


from genshi.builder import tag
from trac.core import *
from trac.perm import IPermissionRequestor
from trac.web.api import IRequestHandler
from trac.web.chrome import (
    INavigationContributor, ITemplateProvider,
    add_stylesheet, add_ctxtnav, add_script,
)
from trac.config import BoolOption, ListOption, IntOption, Option

#from trac.ticket.query import Query

from reports import milestoneReport, componentReport, projectReport,\
    globalReport, userReport
from kanbanindex import kanbanIndex

import time

class KanBanModule(Component):
    implements(INavigationContributor, IRequestHandler, ITemplateProvider,
               IPermissionRequestor)
    
    structured_names = BoolOption('kanbantrac', 'structured_names', True, 
                           doc = "Either components and milestones have\
                           a name like x.y.name where x.y is the project name")
    project_subsections = IntOption('kanbantrac', 'project_subsections', 2,
                                  doc= "Max sublevels of project to show indented on index. All projects are always \
                                  shown, but below this threshold projects are grouped")
    print_conf = BoolOption('kanbantrac', 'print_conf', False,
                            doc = "Print kanbantrac section configuration values used")
    kr_bins = ListOption('kanbantrac','kr_bins',
                         default = 'Backlog, Work in progress, Done',
                         doc = 'Name of each of the colummns on the kanban board and its ticket filter')
    
    kr_default_columns = ListOption('kanbantrac','kr_default_columns',
                         default = 'summary, owner, component',
                         doc = 'Columns shown on tickets tables by default')
    
    new_config_check = IntOption('kanbantrac', 'new_config_check',
                                 default = 600,
                                 doc = 'Interval to check for changes at trac.ini in seconds. If \
                                 on production set it to a high value. If trying configurations, can be set to 1')
    ticket_closed_status = Option('kanbantrac', 'ticket_closed_status',
                                  default = 'closed',
                                  doc = 'Set ticket status for closed tickets. This is used to count open tickets. \
                                  Can be a comma separated list of values')
        
    def __init__(self):
        self._kconf = None
        self._kconf_updated_time = time.time()
    @property
    def kconf(self):
        time_from_update = time.time() - self._kconf_updated_time
        if self._kconf is None or time_from_update > (self.new_config_check):
            self._kconf_updated_time = time.time()
            self._kconf = {}
            for el in self.env.config.options('kanbantrac'):
                self._kconf[el[0]] = el[1]
            
            self._kconf['structured_names'] = self.structured_names
            self._kconf['project_subsections'] = self.project_subsections
            self._kconf['print_conf'] = self.print_conf
            self._kconf['kr_bins'] = self.kr_bins
            self._kconf['kr_default_columns'] = self.kr_default_columns
            self._kconf['new_config_check'] = self.new_config_check
            self._kconf['ticket_closed_status'] = self.ticket_closed_status
        return self._kconf

    # INavigationContributor methods
    def get_active_navigation_item(self, req):
        return 'kanban'

    def get_navigation_items(self, req):
        if req.perm.has_permission('KANBAN_VIEW'):
            yield ('mainnav', 'kanban', tag.a('KanBan',href=req.href.kanbantrac()))

    #IPermissionRequestor methods
    def get_permission_actions(self):
        return ['KANBAN_VIEW']

    # ITemplateProvider methods
    def get_templates_dirs(self):
        from pkg_resources import resource_filename
        return [resource_filename(__name__, 'templates')]

    def get_htdocs_dirs(self):
        from pkg_resources import resource_filename
        return [('hd', resource_filename(__name__, 'htdocs'))]

    # IRequestHandler methods
    def match_request(self, req):
        if not req.perm.has_permission('KANBAN_VIEW'):
            return
        import re
        #self.log.debug("req path: %s" % (req.path_info))
        match = re.match(r'/kanbantrac((/[a-z,A-Z]*)(/.*)?)?$', req.path_info)
        if match:
            #self.log.debug("req fields: %s" % (req.query_string))
            #self.log.debug("req.args: %s" % (str(req.args)))
            if match.group(2) is None:
                req.args['module'] = None
            else:
                req.args['module'] = match.group(2).strip('/')
            if match.group(3) is None:
                req.args['module_call'] = None
            else:
                req.args['module_call'] = match.group(3).strip('/')
            
            #===================================================================
            # if req.query_string:
            #    tmp = req.query_string.split('&')
            #    for el in tmp:
            #        req.args
            #===================================================================
            return True
    
    def process_request(self, req):
        add_stylesheet(req, 'hd/css/kanbantrac.css')

        add_ctxtnav(req, 'Index', req.href.kanbantrac())
        add_ctxtnav(req, 'Global report', req.href.kanbantrac('/global'))
        if self.structured_names: add_ctxtnav(req, 'Project report', req.href.kanbantrac('/project'))
        add_ctxtnav(req, 'Milestone report', req.href.kanbantrac('/milestone'))
        add_ctxtnav(req, 'Component report', req.href.kanbantrac('/component'))
        add_ctxtnav(req, 'User report', req.href.kanbantrac('/user'))
        
        
        
        if self.check_module(req, 'milestone'):
            self.add_table_deps(req)
            template, data = milestoneReport(parent = self, req = req).make()
        elif self.check_module(req, 'component'):
            self.add_table_deps(req)
            template, data = componentReport(parent = self, req = req).make()
        elif self.check_module(req, 'user'):
            self.add_table_deps(req)
            template, data = userReport(parent = self, req = req).make()
        elif self.structured_names and self.check_module(req, 'project'):
            self.add_table_deps(req)
            template, data = projectReport(parent = self, req = req).make()
        elif self.check_module(req, 'global'):
            self.add_table_deps(req)
            template, data = globalReport(parent = self, req = req).make()
        else:
            add_script(req, 'hd/js/jquery-1.8.1.min.js')
            template, data = kanbanIndex(parent = self, req = req).make()
        
        data['foot'] = "page generated by KanBanTrac version %s\n\n" % (VERSION)
        
        if self.print_conf: data['conf'] = self.kconf
        
        return template, data, None
    
    def add_table_deps(self, req):
            add_stylesheet(req, 'hd/css/table/container.css')
            add_stylesheet(req, 'hd/css/table/table.css')
            add_script(req, 'hd/js/jquery-1.8.1.min.js')
            add_script(req, 'hd/js/jquery.dataTables.min.js')
            
    
    def check_module(self, req, mod):
        return (req.args.get('module', '') == mod)
    
    
    