# -*- coding: utf-8 -*-
#
# KanBanTrac
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 3 of the GNU General Public
# License as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA
#
#


from kanbanbase import kanbanbase

class componentReport(kanbanbase):
    

    def __init__(self, parent, req):
        kanbanbase.__init__(self, parent, req)
        self.template = 'component_report.html'
        self.data = {}
         
    def build_data(self):
        if self.req.args['module_call']:
            ## there is one milestone selected
            self.data['component_selected'] = True
            self.data['component'] = self.req.args['module_call']
            self.data['kanban'] = self.kanbanreport(**self.kanban_args)
            self.log.debug('data = %s' % (str(self.data)))
            if self.kconf['structured_names']:
                self.data['components'] = self.components(main=True)
            else:
                self.data['components'] = {}

        else:
            self.data['component_selected'] = False
            self.data['components'] = self.components_dict
            
    def make(self):
        self.build_data()
        
        return self.template, self.data
        
class milestoneReport(kanbanbase):
    

    def __init__(self, parent, req):
        kanbanbase.__init__(self, parent, req)
        self.template = 'milestone_report.html'
        self.data = {}
         
    def build_data(self):
        if self.req.args['module_call']:
            ## there is one milestone selected
            self.data['milestone_selected'] = True
            if self.kconf['structured_names']:
                self.data['components'] = self.components(main = True)
                self.data['component_tickets_foot'] = self.components(related = True, others = True)
            else:
                self.data['components'] = {}
                self.data['component_tickets_foot'] = {}
            self.data['milestone'] = self.req.args['module_call']
            self.data['kanban'] = self.kanbanreport(**self.kanban_args)
        else:
            self.data['milestone_selected'] = False
            self.data['milestones'] = self.milestones_dict
            self.data['components'] = self.components(all_comps=True)
    def make(self):
        self.build_data()
        
        return self.template, self.data
        

class projectReport(kanbanbase):
    

    def __init__(self, parent, req):
        kanbanbase.__init__(self, parent, req)
        self.template = 'project_report.html'
        self.data = {}
         
    def build_data(self):
        if self.req.args['module_call']:
            ## there is one milestone selected
            self.data['project_selected'] = True
            if self.kconf['structured_names']:
                self.data['components'] = self.components(main = True)
                self.data['component_tickets_foot'] = self.components(related = True, others = True)
            else:
                self.data['components'] = {}
                self.data['component_tickets_foot'] = {}
            self.data['project'] = self.req.args['module_call']
            self.data['kanban'] = self.kanbanreport(**self.kanban_args)
        else:
            self.data['project_selected'] = False
            self.data['projects'] = self.projects
            self.data['component_tickets_foot'] = self.components(all_comps=True)
    
    def make(self):
        self.build_data()
        
        return self.template, self.data
        

class globalReport(kanbanbase):
    

    def __init__(self, parent, req):
        kanbanbase.__init__(self, parent, req)
        self.template = 'global_report.html'
        self.data = {}
         
    def build_data(self):
        self.data['component_tickets_foot'] = self.components(all_comps=True)
        self.data['kanban'] = self.kanbanreport(**self.kanban_args)
    
    def make(self):
        self.build_data()
        
        return self.template, self.data

class userReport(kanbanbase):
    

    def __init__(self, parent, req):
        kanbanbase.__init__(self, parent, req)
        self.template = 'user_report.html'
        self.data = {}
         
    def build_data(self):
        if self.req.args['module_call']:
            ## there is one milestone selected
            self.data['user_selected'] = True
            self.data['user'] = self.req.args['module_call']
            self.data['users'] = self.users_dict
            self.data['kanban'] = self.kanbanreport(**self.kanban_args)
        else:
            self.data['user_selected'] = False
            self.data['users'] = self.users_dict
            self.data['component_tickets_foot'] = self.components(all_comps=True)
    
    def make(self):
        self.build_data()
        
        return self.template, self.data

   
    