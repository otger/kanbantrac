# -*- coding: utf-8 -*-
#
# KanBanTrac
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 3 of the GNU General Public
# License as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA
#
#

from genshi.builder import tag

def get_from_db_as_dict(db, rows, table, where = None, ix_row = None):
    sql = "select "
    sql += ", ".join(rows)
    sql += " from " + table
    if where:
        sql += " where %s" %(where)
    
    cursor = db.cursor()
    cursor.execute(sql)
    results = {}
    if ix_row:
        ix_row = rows.index(ix_row)
    else:
        ix_row = 0
        
    for el in cursor:
        t = {}
        for row in rows:
            t[row] = el[rows.index(row)]
        
        results[el[ix_row]] = t
    
    db.commit()
    
    return results

def clause_from_modified(modified):
    import datetime
    import time
    #from datetime import timedelta
    days = 0
    weeks = 0
    if modified.find('day') != -1:
        days = int(modified[:modified.find('day')])
    elif modified.find('week') != -1:
        weeks = int(modified[:modified.find('week')])
    else:
        raise Exception("modified must be either Xday, Xdays, Xweek or Xweeks (where X is an integer). modified is: %s" % (modified))
    td = datetime.timedelta(days = days, weeks = weeks)
    timestamp = time.time() - (td.microseconds + (td.seconds + td.days * 24 * 3600) * 10**6) / 10**6
    
    return "(changetime > %i)" % (int(timestamp * 1000000))
    
def clause_from_filter(ifilter, log = None):
    if log: log.debug("ifilter type and value: %s - %s" %(type(ifilter), ifilter))
    if isinstance(ifilter, basestring):
        ifilter = [ifilter]
    if log: log.debug("ifilter type and value: %s - %s" %(type(ifilter), ifilter))
    
    clauses = []
    for el in ifilter:
        if el.find('!=') != -1:
            eq = '<>'
            name, clause = el.split('!=')
        elif el.find('=') != -1:
            eq = '='
            name,clause = el.split('=')
        else:
            raise Exception("Received clause without = or !=: %s" % (el))
            
        name = name.strip()
        if name == 'modified':
            clauses.append(clause_from_modified(clause.strip()))
        else:
            if clause.find('|') != -1:
                l_clause = clause.split('|')
                junct = ' OR '
            elif clause.find('&') != -1:
                l_clause = clause.split('&')
                junct = ' AND '
            else:
                l_clause = [clause]
                junct = ''
            clauses.append('(' + junct.join([name + ' ' + eq + ' "' + c.strip() + '"' for c in l_clause]) + ')')
    if log: log.debug("clauses type and value: %s - %s" %(type(clauses), clauses))
    return clauses


def micro_to_data(microtimestamp):
    import datetime
    
    return datetime.datetime.fromtimestamp(microtimestamp/1000000).isoformat()


    

