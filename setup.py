# -*- coding: utf-8 -*-
#
# KanBanTrac
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 3 of the GNU General Public
# License as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA
#
#

from setuptools import setup, find_packages
from kanbantrac import VERSION

PACKAGE = 'KanBanTrac'


setup(name=PACKAGE, 
      version=VERSION, 
      author='Otger Ballester',
      author_email='otger@libtronics.com',
      license='GNU GPL v3',
      description='Allows to track milestones tickets in a KanBan board like page',
      url='http://libtronics.com/kanbantrac',
      packages = ['kanbantrac'], 
      entry_points={'trac.plugins': ['%s = kanbantrac.kanbantracmodule' % PACKAGE ]},
      package_data={'kanbantrac': ['templates/*.html',
                                   'htdocs/css/*.css',
                                   'htdocs/css/table/*.css',
                                   'htdocs/img/*.png',
                                   'htdocs/img/table/*.png',
                                   'htdocs/img/tooltips/*.png',
                                   'htdocs/img/*.jpg',
                                   'htdocs/js/*.js']} 
)
